package com.mybank;

import com.mybank.factory.Factory;
import com.mybank.factory.FileFormat;
import com.mybank.model.Account;
import com.mybank.model.AccountStatus;
import com.mybank.model.forms.ClientForm;
import com.mybank.serialization.SerializeAccount;
import com.mybank.serialization.SerializeClient;
import com.mybank.service.Bank;
import com.mybank.model.Client;
import com.mybank.exceptions.*;
import com.mybank.service.BankOperations;
import com.mybank.service.DaoFactory;
import com.mybank.service.impl.BankOperationsImpl;
import com.mybank.service.impl.ClientServiceImpl;

public class MainClass {
    public static void main(String[] args) {
        Bank bank = Bank.getInstance();

        BankOperations operations = new BankOperationsImpl(bank);
        SerializeAccount serializeAcc = new SerializeAccount();
        SerializeClient serializeClient = new SerializeClient();
        ClientServiceImpl clientService = new ClientServiceImpl();

        ClientForm clientForm = new ClientForm();

        clientForm = clientForm.builder()
                .name("karlovych")
                .phone("380682683000")
                .address("Nikolaev")
                .build();


        Client karlovych = clientService.checkin(clientForm);

        System.out.println(karlovych);

        clientForm = clientForm.builder()
                .name("Efremov")
                .address("Nikolaev")
                .phone("380667771345")
                .build();

        Client efremov = clientService.checkin(clientForm);

        clientForm = clientForm.builder()
                .name("Kovalenko")
                .address("Nikolaev")
                .phone("380772779876")
                .build();

        Client kovalenko = clientService.checkin(clientForm);

        Account acc1 = new Account(karlovych, 10000, AccountStatus.Active);
        Account acc2 = new Account(karlovych,777,AccountStatus.Active);
        Account acc3 = new Account(efremov,20000, AccountStatus.Active);
        Account acc4 = new Account(efremov,800,AccountStatus.Active);
        Account acc5 = new Account(kovalenko,2000,AccountStatus.Active);
        Account acc6 = new Account(kovalenko,2000,AccountStatus.Active);

        bank.showAll();

        try{
            Account acc7=new Account(kovalenko,20000,AccountStatus.Active);
            operations.withdraw(acc7,1000);
        } catch (NonExistentAccountException ex) {
            System.out.println(ex.getMessage());
        } catch (NonExistentClientException ex){
            System.out.println(ex.getMessage());
        } catch (WrongBalanceAmountException ex) {
            System.out.println(ex.getMessage());
        } catch (WrongMoneyAmountTransactionException ex) {
            System.out.println(ex.getMessage());
        } catch (OutOfCreditLimitException ex) {
            System.out.println(ex.getMessage());
        } catch (NullPointerException ex) {
            System.out.println(ex.getMessage());
        }

        serializeAcc.serializeAccount(acc6);
        serializeAcc.deserializeAccount(acc6);

        serializeClient.serializeAccount(karlovych);
        serializeClient.deserializeAccount(karlovych);

        Factory factory = new Factory(FileFormat.JSON);
        DaoFactory jsonFactory = factory.createDao(acc1);
        jsonFactory.write("first.json");
        jsonFactory.read("first.json");

        factory = new Factory(FileFormat.XML);
        DaoFactory xmlFactory = factory.createDao(acc2);
        xmlFactory.write("second.xml");
        xmlFactory.read("second.xml");
    }
}


