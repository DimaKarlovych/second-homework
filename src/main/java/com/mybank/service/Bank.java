package com.mybank.service;

import com.mybank.model.Account;
import com.mybank.observer.ObserverImpl;
import lombok.*;

import java.util.*;

@Setter
@Getter
public class Bank {
    private static Bank bank;
    private boolean status = false;
    private Map<String,ArrayList<Account>> lhm = new LinkedHashMap<>();

    ObserverImpl observer = new ObserverImpl();

    private Bank() {}

    public static Bank getInstance() {
        if(bank == null) {
            bank = new Bank();
        }
        bank.status = true;
        return bank;
    }

    public static void addInfo(String name, ArrayList<Account> al) {
        bank.lhm.put(name,al);
    }

    public void showAll() {
        System.out.println("com.mybank.model.Client - ID com.mybank.model.Account: " + lhm.entrySet());
    }

    public void getAccInfo(Account acc) {
        System.out.println(acc);
    }

    public static Map<String, ArrayList<Account>> getLhm() {
        return bank.lhm;
    }

    public void bankStatusBankrupt() {
        status = true;
        observer.notifyObservers();
    }
}