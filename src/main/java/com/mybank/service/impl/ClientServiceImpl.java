package com.mybank.service.impl;

import com.mybank.model.Client;
import com.mybank.model.forms.ClientForm;
import com.mybank.service.AdapterClientFormToClient;
import com.mybank.service.ClientService;

public class ClientServiceImpl implements ClientService {
    static int id = 0;
    @Override
    public Client checkin(ClientForm clientForm) {
        id++;
        clientForm.setId(id);

        AdapterClientFormToClient adapterClientFormToClient = new AdapterClientFormToClient(clientForm);
        return adapterClientFormToClient.adaptClientFormToClient();
    }
}
