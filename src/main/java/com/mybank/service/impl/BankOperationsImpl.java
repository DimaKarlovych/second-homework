package com.mybank.service.impl;

import com.mybank.exceptions.NonExistentBankException;
import com.mybank.model.Account;
import com.mybank.service.Bank;
import com.mybank.service.BankOperations;
import com.mybank.validators.AccountValidator;

public class BankOperationsImpl implements BankOperations {
    private Bank bank;
    public BankOperationsImpl(Bank bank) {
        this.bank = bank;
    }

    private void checkOperation(Account account, double quantity) {
        AccountValidator validator = new AccountValidator();
        validator.validateAccount(account,quantity);
        if(this.bank == null) {
            throw new NonExistentBankException();
        }
    }

    public void withdraw(Account account, double amount) {
        checkOperation(account, amount);
        AccountValidator validator = new AccountValidator();
        validator.validateWithdraw(account, amount);
        account.setMoney(account.getMoney() - amount);
    }

    public void deposit(Account account,double amount) {
        checkOperation(account, amount);
        AccountValidator validator = new AccountValidator();
        validator.validateDeposit(account, amount);
        account.setMoney(account.getMoney() + amount);
    }
}
