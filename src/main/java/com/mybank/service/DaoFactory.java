package com.mybank.service;

public interface DaoFactory {
    void write(String path);
    void read(String path);
}
