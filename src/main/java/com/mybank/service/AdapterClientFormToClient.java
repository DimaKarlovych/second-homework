package com.mybank.service;

import com.mybank.model.Client;
import com.mybank.model.forms.ClientForm;

public class AdapterClientFormToClient {
    private ClientForm clientForm;

    public AdapterClientFormToClient(ClientForm clientForm) {
        this.clientForm = clientForm;
    }

    public Client adaptClientFormToClient() {
        Client client = new Client(clientForm.getName(),
                clientForm.getAddress(), clientForm.getPhone(), clientForm.getId());
        return client;
    }
}
