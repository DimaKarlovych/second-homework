package com.mybank.service;

import com.mybank.model.Client;
import com.mybank.model.forms.ClientForm;

public interface ClientService {
    public Client checkin(ClientForm clientForm);
}
