package com.mybank.service;

import com.mybank.model.Account;

public interface BankOperations{
    void deposit(Account acc, double payment);
    void withdraw(Account acc, double withdrawal);
}
