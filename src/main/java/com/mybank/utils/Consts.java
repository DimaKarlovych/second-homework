package com.mybank.utils;

public class Consts {
    public final static String ACCOUNT_BLOCKED_EXCEPTION = "Account blocked exception";
    public final static String NON_EXISTENT_BANK_EXCEPTION = "Non existent bank exception";
    public final static String OUT_OF_CREDIT_LIMIT_EXCEPTION = "Out of credit limit exception";
    public final static String OVERFLOW_LIMIT_EXCEPTION = "Overflow limit exception";
    public final static String NON_EXISTENT_ACCOUNT_EXCEPTION = "Non existent account exception";
    public final static String WRONG_BALANCE_AMOUNT_EXCEPTION = "Wrong balance amount exception";
    public final static String WRONG_MONEY_AMOUNT_TRANSACTION_EXCEPTION = "Wrong money amount transaction exception";
    public final static String NON_EXISTENT_CLIENT_EXCEPTION = "Non existent client exception";
}
