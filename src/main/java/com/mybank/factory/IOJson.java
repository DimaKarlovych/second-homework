package com.mybank.factory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mybank.model.Account;
import com.mybank.service.DaoFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedHashMap;

public class IOJson implements DaoFactory {
    private Account account;
    public IOJson(Account account) {
        this.account = account;
    }

    @Override
    public void write(String path) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            objectMapper.writeValue(new File(path), account);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void read(String path) {
        LinkedHashMap result = null;
        try {
            byte[] mapData = Files.readAllBytes(Paths.get(path));
            ObjectMapper objectMapper = new ObjectMapper();
            result = objectMapper.readValue(mapData, LinkedHashMap.class);
        } catch (JsonProcessingException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(result);
    }
}
