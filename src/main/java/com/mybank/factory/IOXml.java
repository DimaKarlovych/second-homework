package com.mybank.factory;

import com.mybank.model.Account;
import com.mybank.service.DaoFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class IOXml implements DaoFactory {
    private Account account;

    public IOXml(Account account) {
        this.account = account;
    }
    @Override
    public void write(String path) {
        try
        {
            JAXBContext jaxbContext = JAXBContext.newInstance(Account.class);

            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            File file = new File(path);

            jaxbMarshaller.marshal(account, file);
        }
        catch (JAXBException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void read(String fileName) {

        File xmlFile = new File(fileName);

        JAXBContext jaxbContext;
        try
        {
            jaxbContext = JAXBContext.newInstance(Account.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            Account account = (Account) jaxbUnmarshaller.unmarshal(xmlFile);

            System.out.println(account);
        }
        catch (JAXBException e)
        {
            e.printStackTrace();
        }
    }

}
