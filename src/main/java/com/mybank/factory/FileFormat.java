package com.mybank.factory;

public enum FileFormat {
    JSON,
    XML
}
