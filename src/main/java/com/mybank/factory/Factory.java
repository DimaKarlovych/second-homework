package com.mybank.factory;

import com.mybank.model.Account;
import com.mybank.service.DaoFactory;

public class Factory {
    FileFormat format;

    public Factory(FileFormat format) {
        this.format = format;
    }

    public DaoFactory createDao(Account account) {
        switch (this.format) {
            case XML:
                return new IOXml(account);
            default: JSON:
                return new IOJson(account);
        }
    }
}
