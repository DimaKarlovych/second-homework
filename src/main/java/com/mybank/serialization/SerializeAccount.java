package com.mybank.serialization;

import com.mybank.model.Account;
import java.io.*;

public class SerializeAccount implements Serializable {
    private static final long SERIAL_VERSION_UID = 1L;

    public void serializeAccount(Account account) {
        ObjectOutputStream objectOutputStream = null;
        try {
            try {
                FileOutputStream fileOutputStream = new FileOutputStream
                        ("/home/dima/IdeaProjects/homework2/accountSerialize.ser");
                objectOutputStream = new ObjectOutputStream(fileOutputStream);
                objectOutputStream.writeObject(account);
            } catch (FileNotFoundException ex) {
                System.out.println(ex.getMessage());
            } finally {
                objectOutputStream.close();
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void deserializeAccount(Account account) {
        try {
            FileInputStream fileInputStream = new FileInputStream
                    ("/home/dima/IdeaProjects/homework2/accountSerialize.ser");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            account = (Account) objectInputStream.readObject();
            System.out.println(account);
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
