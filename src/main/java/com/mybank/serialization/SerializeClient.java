package com.mybank.serialization;

import com.mybank.model.Client;
import java.io.*;

public class SerializeClient {
    private static final long SERIAL_VERSION_UID = 1L;

    public void serializeAccount(Client client) {
        ObjectOutputStream objectOutputStream = null;
        try {
            try {
                FileOutputStream fileOutputStream = new FileOutputStream
                        ("/home/dima/IdeaProjects/homework2/clientSerialize.ser");
                objectOutputStream = new ObjectOutputStream(fileOutputStream);
                objectOutputStream.writeObject(client);
            } catch (FileNotFoundException ex) {
                System.out.println(ex.getMessage());
            } finally {
                objectOutputStream.close();
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void deserializeAccount(Client client) {
        try {
            FileInputStream fileInputStream = new FileInputStream
                    ("/home/dima/IdeaProjects/homework2/clientSerialize.ser");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            client = (Client) objectInputStream.readObject();
            System.out.println(client);
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
