package com.mybank.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import com.mybank.observer.Observer;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.xml.bind.annotation.*;
import java.io.Serializable;

@Getter
@Setter
@JsonIgnoreProperties(value = { "client", "clientInfo" })
@XmlRootElement(name = "account")
@XmlAccessorType(XmlAccessType.FIELD)
public class Account implements Serializable, Observer {

    private static int counterAccounts = 0;
    private int id;
    private double money;
    private AccountStatus accountStatus;
    private String clientInfo;

    @XmlTransient
    private Client client;

    public Account() {}

    public Account(@NonNull Client client, double money, AccountStatus accountStatus) {
        id = ++counterAccounts;
        this.client = client;
        this.money = money;
        this.accountStatus = accountStatus;
        clientInfo="Address: " + client.getAddress() + " , Phone: " + client.getPhone();
        client.addAccount(this);
    }

    @Override
    public String toString() {
        return "com.mybank.model.Account{" +
                "id=" + id +
                ", money=" + money +
                ", accountStatus=" + accountStatus+"}";
    }

    @Override
    public void handleEvent() {
        setMoney(0);
        System.out.println(getClient()+" Balance: "+ getMoney());
    }
}