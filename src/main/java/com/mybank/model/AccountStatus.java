package com.mybank.model;

import lombok.Getter;

@Getter
public enum AccountStatus{
    Active, NonActive, Blocked;
}
