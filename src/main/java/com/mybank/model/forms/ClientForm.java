package com.mybank.model.forms;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ClientForm {
    private int id;
    private String name;
    private String address;
    private String phone;
}
