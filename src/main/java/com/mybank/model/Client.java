package com.mybank.model;

import com.mybank.service.Bank;
import lombok.*;

import java.io.Serializable;
import java.util.ArrayList;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Client implements Serializable {
    private String name;
    private String address;
    private String phone;
    private int id;

    private ArrayList<Account> al = new ArrayList<>();

    public Client(String name, String address, String phone, int id) {
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.id = id;
    }

    void addAccount(Account acc) {
        al.add(acc);
        Bank.addInfo(name,al);
    }

    public void showClientAccounts() {
        for(Account acc : al){
            System.out.println(acc);
        }
    }

    @Override
    public String toString() {
        return "Client{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                ", id=" + id +
                '}';
    }
}
