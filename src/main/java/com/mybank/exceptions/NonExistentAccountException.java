package com.mybank.exceptions;

import static com.mybank.utils.Consts.NON_EXISTENT_ACCOUNT_EXCEPTION;

public class NonExistentAccountException extends RuntimeException {
    public NonExistentAccountException() {
        super(NON_EXISTENT_ACCOUNT_EXCEPTION);
    }

    public NonExistentAccountException(String message) {
        super(message);
    }

    public NonExistentAccountException(String message, Throwable cause) {
        super(message, cause);
    }
}
