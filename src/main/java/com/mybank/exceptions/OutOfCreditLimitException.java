package com.mybank.exceptions;

import static com.mybank.utils.Consts.OUT_OF_CREDIT_LIMIT_EXCEPTION;

public class OutOfCreditLimitException extends RuntimeException {
    public OutOfCreditLimitException() {
        super(OUT_OF_CREDIT_LIMIT_EXCEPTION);
    }

    public OutOfCreditLimitException(String message) {
        super(message);
    }

    public OutOfCreditLimitException(String message, Throwable cause) {
        super(message, cause);
    }
}
