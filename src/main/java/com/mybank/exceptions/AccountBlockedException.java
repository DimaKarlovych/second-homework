package com.mybank.exceptions;

import static com.mybank.utils.Consts.ACCOUNT_BLOCKED_EXCEPTION;

public class AccountBlockedException extends RuntimeException {
    public AccountBlockedException() {
        super(ACCOUNT_BLOCKED_EXCEPTION);
    }

    public AccountBlockedException(String message) {
        super(message);
    }

    public AccountBlockedException(String message, Throwable cause) {
        super(message, cause);
    }
}
