package com.mybank.exceptions;

import static com.mybank.utils.Consts.NON_EXISTENT_CLIENT_EXCEPTION;

public class NonExistentClientException extends RuntimeException{
    public NonExistentClientException() {
        super(NON_EXISTENT_CLIENT_EXCEPTION);
    }

    public NonExistentClientException(String message) {
        super(message);
    }

    public NonExistentClientException(String message, Throwable cause) {
        super(message, cause);
    }
}
