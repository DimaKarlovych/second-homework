package com.mybank.exceptions;

import static com.mybank.utils.Consts.NON_EXISTENT_BANK_EXCEPTION;
public class NonExistentBankException extends RuntimeException {
    public NonExistentBankException() {
        super(NON_EXISTENT_BANK_EXCEPTION);
    }

    public NonExistentBankException(String message) {
        super(message);
    }

    public NonExistentBankException(String message, Throwable cause) {
        super(message, cause);
    }
}

