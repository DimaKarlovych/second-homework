package com.mybank.exceptions;

import static com.mybank.utils.Consts.WRONG_BALANCE_AMOUNT_EXCEPTION;

public class WrongBalanceAmountException extends RuntimeException {
    public WrongBalanceAmountException() {
        super(WRONG_BALANCE_AMOUNT_EXCEPTION);
    }

    public WrongBalanceAmountException(String message) {
        super(message);
    }

    public WrongBalanceAmountException(String message, Throwable cause) {
        super(message, cause);
    }

}
