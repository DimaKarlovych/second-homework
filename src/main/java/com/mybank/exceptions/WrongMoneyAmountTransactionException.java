package com.mybank.exceptions;

import static com.mybank.utils.Consts.WRONG_MONEY_AMOUNT_TRANSACTION_EXCEPTION;

public class WrongMoneyAmountTransactionException extends RuntimeException {
    public WrongMoneyAmountTransactionException() {
        super(WRONG_MONEY_AMOUNT_TRANSACTION_EXCEPTION);
    }

    public WrongMoneyAmountTransactionException(String message) {
        super(message);
    }

    public WrongMoneyAmountTransactionException(String message, Throwable cause) {
        super(message, cause);
    }

}
