package com.mybank.exceptions;

import static com.mybank.utils.Consts.OVERFLOW_LIMIT_EXCEPTION;

public class OverflowLimitException extends RuntimeException {
    public OverflowLimitException() {
        super(OVERFLOW_LIMIT_EXCEPTION);
    }

    public OverflowLimitException(String message) {
        super(message);
    }

    public OverflowLimitException(String message, Throwable cause) {
        super(message, cause);
    }
}
