package com.mybank.validators;

import com.mybank.exceptions.*;
import com.mybank.model.Account;
import com.mybank.model.AccountStatus;

public class AccountValidator {
    public void validateAccount(Account acc, double amount) {
        if(acc.getAccountStatus() == AccountStatus.Blocked) {
            throw new AccountBlockedException();
        }

        if(acc.getClient() == null) {
            throw new NonExistentClientException();
        }

        if (acc.getMoney() < 0) {
            throw new WrongBalanceAmountException();
        }

        if(amount < 0) {
            throw new WrongMoneyAmountTransactionException();
        }
    }

    public void validateWithdraw(Account acc, double amount) {
        double dif = acc.getMoney()-amount;
        if(dif<0) {
            throw new OutOfCreditLimitException();
        }
    }

    public void validateDeposit(Account acc, double amount) {
        double dif = acc.getMoney()+amount;
        if(dif>99999) {
            throw new OverflowLimitException();
        }
    }
}
