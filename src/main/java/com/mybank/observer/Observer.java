package com.mybank.observer;

public interface Observer {
    public void handleEvent();
}
