package com.mybank.observer;

import com.mybank.model.Account;

import java.util.ArrayList;
import java.util.List;

public class ObserverImpl implements Observed {
    private List<Account> list = new ArrayList<>();

    @Override
    public void addObserver(Account account) {
        list.add(account);
    }

    @Override
    public void removeObserver(Account account) {
        list.remove(account);
    }

    @Override
    public void notifyObservers() {
        System.out.println("Bank is bankrupt !!!");
        for (Observer observer : list) {
            observer.handleEvent();
        }
    }
}
