package com.mybank.observer;

import com.mybank.model.Account;

public interface Observed {
    public void addObserver(Account observer);

    public void removeObserver(Account observer);

    public void notifyObservers();
}
